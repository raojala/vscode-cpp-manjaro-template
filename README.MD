# C++ VSCode template for Manjaro linux

1. Create folders bin/debug and bin/release (repository doesn't like empty
folders). These are required for tasks to work properly
2. All source and header files must be in the workspace folder root.
WTB virtual folders.
3. Pre made tasks are Debug (default f5), Release that you have to run
with ctrl+shift+p >run task and finally a Cleanup Debug task that removes
.out file in bin/debug.

## Notes

- Adding command line arguments for debugging goes to .vscode/launch.json,
under "name" : "Debug", "args":[],
- Release task outputs a .out file with the name of workspace folder name
- "cppStandard" is set to "c++17"
- g++ compiles with -std=c++17 flag

## Dependencies

- Tasks are using g++ compiler
- Debugger is using GDB

## Install required software

    sudo pacman -S base-devel --needed
    sudo pacman -S gdb --needed

If packman doesn't find gdb, graphical interface did for me.